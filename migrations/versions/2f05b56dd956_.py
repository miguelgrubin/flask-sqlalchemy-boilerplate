"""empty message

Revision ID: 2f05b56dd956
Revises: 
Create Date: 2019-07-31 01:04:05.182335

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '2f05b56dd956'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'places',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('name', sa.String(120), nullable=False, unique=True),
        sa.Column('created_at', sa.DateTime),
        sa.Column('updated_at', sa.DateTime),
    )


def downgrade():
    op.drop_table('places')
