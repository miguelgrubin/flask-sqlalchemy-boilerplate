
# -*- coding: utf-8 -*-
"""Factories to help in tests."""
from factory import PostGenerationMethodCall, Sequence
from factory.alchemy import SQLAlchemyModelFactory

from apidtango.extensions import db
from apidtango.places.models import Place


class BaseFactory(SQLAlchemyModelFactory):
    """Base factory."""

    class Meta:
        """Factory configuration."""

        abstract = True
        sqlalchemy_session = db.session


class PlaceFactory(BaseFactory):
    """Place factory."""

    name = Sequence(lambda n: 'place{0}'.format(n))

    class Meta:
        """Factory configuration."""

        model = Place
