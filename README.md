# Guidelines:
 - Use Flask
 - Use flask_restplus to expose API documentation on Swagger
 - Use SQLAlchemy
 - Use Alembic to manage DB migrations.
 - Seeds DB
 - Markdown documentation 
 - Unit testing with pytest
