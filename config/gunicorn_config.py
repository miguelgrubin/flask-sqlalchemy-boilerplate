import os

addr = os.environ.get("HTTP_ADDR", "0.0.0.0")
port = os.environ.get("HTTP_PORT", "8000")
loglevel = os.environ.get("LOG_LEVEL", "info")
app_env = os.environ.get("FLASK_ENV", "production")

reload = False
bind = "{0}:{1}".format(addr, port)

if app_env == "development":
    reload = True
