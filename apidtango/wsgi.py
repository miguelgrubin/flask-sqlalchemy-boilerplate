"""WSGI entrypoint."""
import os

from apidtango import app, settings


def get_running_conf():
    """Select running config from FLASK_ENV."""
    env = os.getenv("FLASK_ENV")
    if env == "testing":
        return settings.TestConfig
    if env == "development":
        return settings.DevConfig
    return settings.ProdConfig


application = app.create_app(get_running_conf())

if __name__ == "__main__":
    application.run()
