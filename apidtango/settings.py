"""Application configuration."""
import os


DATABASE_URI = "{conn}://{user}:{passwd}@{host}:{port}/{name}".format(
    conn=os.getenv("DB_CONN", default="postgresql"),
    user=os.getenv("DB_USER", default=None),
    passwd=os.getenv("DB_PASS", default=None),
    host=os.getenv("DB_HOST", default="localhost"),
    port=os.getenv("DB_PORT", default="5432"),
    name=os.getenv("DB_NAME", default="app"),
)


class Config:
    """Base configuration."""

    APP_DIR = os.path.abspath(os.path.dirname(__file__))
    PROJECT_ROOT = os.path.abspath(os.path.join(APP_DIR, os.pardir))
    DEBUG_TB_INTERCEPT_REDIRECTS = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_DATABASE_URI = DATABASE_URI


class ProdConfig(Config):
    """Production configuration."""

    ENV = "prod"
    DEBUG = False


class DevConfig(Config):
    """Development configuration."""

    ENV = "dev"
    DEBUG = True


class TestConfig(Config):
    """Test configuration."""

    TESTING = True
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = "sqlite://"
