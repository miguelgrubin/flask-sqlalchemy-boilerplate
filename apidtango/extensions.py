"""Extensions module. Each extension is initialized in the app factory located in app.py."""

from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_restplus import Api

db = SQLAlchemy()
migrate = Migrate()
cors = CORS(resources={r"/v1/*": {"origins": "*"}})
api = Api(version="1.0", title="API", description="A simple API")
