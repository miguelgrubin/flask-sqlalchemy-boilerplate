"""App factory."""
from flask import Flask

from apidtango import commands, places
from apidtango.extensions import db, migrate, cors, api
from apidtango.settings import ProdConfig


def create_app(config_object=ProdConfig):
    """Create Flask app with config applied and routes appended."""
    app = Flask(__name__.split(".")[0])
    app.url_map.strict_slashes = False
    app.config.from_object(config_object)
    register_extensions(app)
    register_namespaces()
    register_commands(app)
    register_shellcontext(app)
    return app


def register_extensions(app):
    """Register Flask extensions."""
    api.init_app(app)
    db.init_app(app)
    migrate.init_app(app, db)
    cors.init_app(app)


def register_namespaces():
    """Register Flask Restplus Namespaces."""
    api.add_namespace(places.views.ns)


def register_commands(app):
    """Register Click commands."""
    app.cli.add_command(commands.seed)


def register_shellcontext(app):
    """Register shell context objects."""

    def shell_context():
        """Shell context objects."""
        return {"db": db, "Place": places.models.Place}

    app.shell_context_processor(shell_context)
