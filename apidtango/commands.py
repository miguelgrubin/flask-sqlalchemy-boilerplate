"""Click commands."""
import click
from flask.cli import with_appcontext
from sqlalchemy import func

from apidtango.extensions import db
from apidtango.places.models import Place


@click.command()
@with_appcontext
def seed():
    """Run place seeding"""
    click.echo("Seeding DB with Places...")
    places = [
        Place(name="Reino del Norte"),
        Place(name="Reino de la Montaña y el Valle"),
        Place(name="Reino de las Islas y los Ríos"),
        Place(name="Reino de la Roca"),
        Place(name="Reino del Dominio"),
        Place(name="Reino de las Tormentas"),
        Place(name="Principado de Dorne"),
    ]
    try:
        db.session.add_all(places)
        db.session.commit()
    except Exception as _e:
        db.session.rollback()
    count_places = db.session.query(func.count("*")).select_from(Place).scalar()
    click.echo("Total Places: {count}".format(count=count_places))
    exit(0)
