"""Models for Place module"""
import datetime as dt

from apidtango.extensions import db


class Place(db.Model):
    """docstring for Place"""

    __tablename__ = "places"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120), unique=True, nullable=False)
    created_at = db.Column(db.DateTime, nullable=False, default=dt.datetime.utcnow)
    updated_at = db.Column(db.DateTime, nullable=False, default=dt.datetime.utcnow)

    def __init__(self, name):
        db.Model.__init__(self, name=name)

    def __repr__(self):
        return self.name
