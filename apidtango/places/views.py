"""Places API path"""
from flask_restplus import Namespace, Resource

from apidtango.extensions import api, db
from .serializers import place_serializer, place_modificable
from .models import Place


ns = Namespace("places", description="Places operations", path="/v1/places")


@ns.route("/")
class PlaceResourceList(Resource):
    """Resource for list and create places."""

    @ns.marshal_list_with(place_serializer)
    def get(self):
        """Get all places."""
        return Place.query.all()

    @ns.expect(place_modificable)
    @ns.marshal_with(place_serializer, code=201)
    def post(self):
        """Create one place."""
        place = Place(**api.payload)
        try:
            db.session.add(place)
            db.session.commit()
            return place
        except Exception as _e:
            db.session.rollback()
            return "Invalid Place request", 401


@ns.route("/<int:place_id>")
class PlaceResource(Resource):
    """Resource for get, update and remove a place."""

    @ns.marshal_with(place_serializer)
    def get(self, place_id):
        """Get a place or response not-found."""
        place = Place.query.get(place_id)
        if place:
            return place
        return "Place {} doesn't exist".format(place_id), 404

    @ns.expect(place_modificable)
    @ns.marshal_with(place_serializer, code=201)
    def patch(self, place_id):
        """Update a place name or response not-found."""
        place = Place.query.get(place_id)
        if place:
            db.session.query(Place).filter(Place.id == place_id).update(api.payload)
            db.session.commit()
            return place
        return "Place {} doesn't exist".format(place_id), 404

    @ns.response(204, "Place deleted")
    def delete(self, place_id):
        """Delete a place or response not-found."""
        place = Place.query.get(place_id)
        if place:
            db.session.delete(place)
            db.session.commit()
            return "", 204
        return "Place {} doesn't exist".format(place_id), 404
