"""Serializers for requests and responses body."""
from flask_restplus import fields

from apidtango.extensions import api

place_serializer = api.model("Model", {"id": fields.Integer, "name": fields.String})

place_modificable = api.model("Model", {"name": fields.String})
