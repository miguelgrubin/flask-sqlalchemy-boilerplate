FROM python:3.7

ENV FLASK_ENV production
ENV FLASK_APP /app/src/apidtango
WORKDIR /app/src
RUN pip install pipenv

ADD ./Pipfile ./Pipfile.lock ./
RUN pipenv install --system --dev
ADD . .

CMD [ "pipenv", "run", "gunicorn", "--config", "./config/gunicorn_config.py", "apidtango.wsgi" ]