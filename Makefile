.PHONY: clean-pyc
TARGET = apidtango
ENVIRONMENT = development
DOCKER_COMPOSE := docker-compose -f docker/docker-compose.$(ENVIRONMENT).yml
DOCKER_RUN := $(DOCKER_COMPOSE) run --rm $(TARGET)

all: test

build: clean-pyc
	$(DOCKER_COMPOSE) build

server: clean-pyc
	$(DOCKER_COMPOSE) up

sh:
	$(DOCKER_RUN) bash

test: clean-pyc
	ENVIRONMENT=testing
	$(DOCKER_RUN) pytest

coverage:
	$(DOCKER_RUN) make coverage-report

coverage-report: clean-pyc
	coverage run -m pytest
	coverage report
	coverage html

docs: clean-pyc
	$(DOCKER_RUN) $(MAKE) -C docs html

lint:
	$(DOCKER_RUN) pylint $(TARGET)

black:
	$(DOCKER_RUN) black $(TARGET)

maintainability:
	$(DOCKER_RUN) make maintainability-report

maintainability-report:
	@echo "\n\n|======->  Maintainability  <-======|"
	radon mi $(TARGET)
	@echo "\n\n|======->  Cyclomatic Complexity  <-======|"
	radon cc $(TARGET)

clean-pyc:
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +
	find . -name '__pycache__' -exec rm -d {} +
